﻿USE practica1;

-- 01 todos los registros de la tabla empleado

  SELECT * FROM empleado;

-- 02 todos los registros de la tabla departamento

  SELECT * FROM departamento;

-- 03 apellido y oficio de cada empleado

  SELECT apellido,oficio FROM empleado;

-- 04 apellido y oficio de cada empleado

  SELECT loc,dept_no FROM departamento;

-- 05 número, nombre y localización de cada departamento

  SELECT dnombre, dept_no, loc FROM departamento;
  
-- 06 número de empleados que hay

  SELECT COUNT(emp_no) FROM empleado;

-- 07 empleados mostrados por orden ascendente de apellido

  SELECT * FROM empleado ORDER BY(apellido) ASC;

-- 08 empleados mostrados por orden descendente de apellido

  SELECT * FROM empleado ORDER BY(apellido) DESC;

-- 09 número de departamentos que hay

  SELECT COUNT(dept_no) FROM departamento;

-- 10 indicar numero de empleados más número de departamentos

  SELECT COUNT(dept_no) + COUNT(emp_no) AS Total FROM empleado;

-- 11 datos de los empleados ordenados por número de departamento descendentemente

  SELECT * FROM empleado ORDER BY dept_no DESC;

-- 12 datos de los empleados ordenados por número de departamento descendentemente y por oficio adcendente

  SELECT * FROM empleado ORDER BY dept_no DESC, oficio ASC;

-- 13 datos de los empleados ordenados por número de departamento descendentemente y por apellido ascendentemente

  SELECT * FROM empleado ORDER BY dept_no DESC, apellido ASC;

-- 14 mostrar los codigos de los empleados cuyo salario sea mayor que 2000

  SELECT emp_no FROM empleado WHERE salario>2000;

-- 15 mostrar los codigos y apellidos de los empleados cuyo salario sea menor que 2000

   SELECT emp_no,apellido FROM empleado WHERE salario<2000;

-- 16 mostrar los datos de los empleados cuyo salario este entre 1500 y 2500

   SELECT * FROM empleado WHERE salario BETWEEN 1500 AND 2500;

-- 17 mostrar los datos de los empleados cuyo oficio sea analista

   SELECT * FROM empleado WHERE oficio='analista';

-- 18 mostrar los datos de los empleados cuyo oficio sea analista y ganen más de 2000

   SELECT * FROM empleado WHERE oficio='analista' AND salario>2000;

-- 19 seleccionar el apellido y oficio de los empleados del departamento numero 20

   SELECT apellido,oficio FROM empleado WHERE dept_no=20;

-- 20 contar el número de empleados cuyo oficio sea vendedor
  
   SELECT COUNT(*) AS EmpleadosVendedores FROM empleado WHERE oficio='vendedor';

-- 21 mostrar todos los datos de los empleados cuyos apellidos comiencen por m o por n ordenados por apellido de forma ascendente

   SELECT * FROM empleado WHERE (apellido LIKE 'm%' OR apellido LIKE 'n%') ORDER BY apellido ASC;

-- 22 seleccionar los empleados cuyo oficio sea vendedor. mostrar los datos ordenados por apellido de forma ascendente

   SELECT * FROM empleado WHERE oficio='vendedor' ORDER BY apellido ASC;

-- 23 mostrar los apellidos del empleado que mas gana

   SELECT apellido FROM empleado GROUP BY apellido HAVING MAX(salario) ORDER BY salario DESC LIMIT 1;

-- 24 mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea analista. ordenar por apellido y oficio ascendentemente

   SELECT * FROM empleado WHERE dept_no=10 AND oficio='analista' ORDER BY apellido ASC, oficio ASC;

-- 25 realizar un listado de los distintos meses en que los empleados se han dado de alta

   SELECT DISTINCT(MONTH(fecha_alt)) FROM empleado;

-- 26 realizar un listado de los distintos años en que los empleados se han dado de alta

   SELECT DISTINCT(YEAR(fecha_alt)) FROM empleado;

-- 27 realizar un listado de los distintos días del mes en que los empleados se han dado de alta

   SELECT DISTINCT(DAY(fecha_alt)) FROM empleado;

-- 28 mostrar los apellidos de los empleados que tengan un salario mayor que 2000 o que pertenezcan al departamento 20

   SELECT apellido FROM empleado WHERE salario>2000 OR dept_no=20;

-- 29 realizar un listado donde nos coloque el apellido del empleado y el nombre del departamento al que pertenece

   SELECT apellido, dnombre FROM empleado JOIN departamento ON empleado.dept_no=departamento.dept_no;

-- 30 realizar un listado donde nos coloque el apellido del empleado, el oficio del empleado y el nombre del departamento al que pertence. ordenar por apellido descendente

   SELECT apellido, oficio, dnombre FROM empleado JOIN departamento ON empleado.dept_no=departamento.dept_no ORDER BY apellido DESC;

-- 31 listar el número de empleados por departamento. la salida del comando debe ser como la que vemos (order dept_no asc)
  
   SELECT departamento.dept_no, COUNT(apellido) AS NumeroEmpleados FROM empleado JOIN departamento ON empleado.dept_no=departamento.dept_no GROUP BY departamento.dept_no ORDER BY departamento.dept_no ASC;

-- 32 realizar el mismo comando anterior pero obteniendo una salida como la que vemos

   SELECT departamento.dnombre, COUNT(apellido) AS NumeroEmpleados FROM empleado JOIN departamento ON empleado.dept_no=departamento.dept_no GROUP BY departamento.dnombre ORDER BY NumeroEmpleados ASC;

-- 33 listar el apellido de todos los empleados y ordenarlos por oficio y nombre

  SELECT apellido FROM empleado JOIN departamento ON empleado.dept_no=departamento.dept_no ORDER BY oficio ASC, dnombre ASC;

-- 34 seleccionar de la tabla empleado los empleados cuyo apellido empiece por 'a'. listar apellidos

  SELECT apellido FROM empleado WHERE apellido LIKE 'a%';

-- 35 seleccionar de la tabla empleado los empleados cuyo apellido empiece por 'a' o por 'm'. listar apellidos

  SELECT apellido FROM empleado WHERE (apellido LIKE 'a%' OR apellido LIKE 'm%');

-- 36 seleccionar de la tabla empleado los empleados cuyo apellido no termine por 'z'. listar todos los campos de la tabla

  SELECT * FROM empleado WHERE apellido NOT LIKE '%z';

-- 37 seleccionar de la tabla empleado aquellas filas cuyo apellido empiece por 'a' y el oficio tenga una 'e' en cualquier posicion. ordenar la salida por oficio y por salario descendente

  SELECT * FROM empleado WHERE (apellido LIKE 'a%' AND oficio LIKE '%e%') ORDER BY oficio DESC, salario DESC;

