﻿CREATE DATABASE IF NOT EXISTS practica2;
USE practica2;

CREATE OR REPLACE TABLE Ciudad (
  nombre Varchar(30) PRIMARY KEY,
  población Integer
);

CREATE OR REPLACE TABLE Persona (
  nombre Varchar(30) PRIMARY KEY,
  calle Varchar(30),
  ciudad Varchar(30)
 );

CREATE OR REPLACE TABLE Compañia (
  nombre Varchar(30) PRIMARY KEY,
  ciudad Varchar(30)
);

CREATE OR REPLACE TABLE Trabaja (
  persona Varchar(30) PRIMARY KEY,
  compañia Varchar(30),
  salario Integer
);

CREATE OR REPLACE TABLE Supervisa (
  supervisor Varchar(30),
  persona Varchar(30),
  PRIMARY KEY (supervisor,persona)
);

