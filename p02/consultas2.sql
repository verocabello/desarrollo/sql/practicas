﻿USE practica2;

-- 01 número de ciudades que hay en la tabla ciudades

  SELECT COUNT(*) AS TotalCiudades FROM ciudad;

-- 02 nombre de las ciudades que tengan una población por encima de la población media
  
  SELECT AVG(población) FROM ciudad;

  SELECT nombre FROM ciudad WHERE población>(SELECT AVG(población) FROM ciudad);

-- 03 nombre de las ciudades que tengan una población por debajo de la población media
    
  SELECT AVG(población) FROM ciudad;

  SELECT nombre FROM ciudad WHERE población<(SELECT AVG(población) FROM ciudad);

-- 04 nombre de la ciudad con la población maxima
  
  SELECT MAX(población) FROM ciudad;

  SELECT nombre FROM ciudad WHERE población=(SELECT MAX(población) FROM ciudad);

-- 05 nombre de la ciudad con la población minima

  SELECT MIN(población) FROM ciudad;

  SELECT nombre FROM ciudad WHERE población=(SELECT MIN(población) FROM ciudad);

-- 06 numero de ciudades que tengan una población por encima de la población media

  SELECT AVG(población) FROM ciudad;

  SELECT COUNT(*) FROM ciudad WHERE población>(SELECT AVG(población) FROM ciudad);

-- 07 numero de personas que viven en cada ciudad

  SELECT COUNT(nombre) AS NumeroPersonas, ciudad FROM persona GROUP BY ciudad;

-- 08 con tabla trabaja, cuantas personas trabajan en cada una de las compañias

  SELECT COUNT(persona) AS NumeroPersonas, compañia FROM trabaja GROUP BY compañia;

-- 09 compañia que mas trabajadores tiene

  SELECT COUNT(*) AS NumeroPersonas, compañia FROM trabaja GROUP BY compañia ORDER BY NumeroPersonas DESC LIMIT 1;

-- 10 salario medio de cada una de las compañias

  SELECT compañia, AVG(salario) FROM trabaja GROUP BY compañia;

-- 11 nombre de las personas y población de la ciudad donde viven

  SELECT persona.nombre, ciudad.población FROM persona JOIN ciudad ON persona.ciudad=ciudad.nombre;
  
-- 12 nombre de las personas, calle donde vive y la poblacion de la ciudad donde vive
  
  SELECT persona.nombre, persona.calle, ciudad.población FROM persona
    JOIN ciudad ON (persona.ciudad=ciudad.nombre);

-- 13 nombre de las personas, la ciudad donde vive y la ciudad donde está la compañia para la que trabaja

  SELECT trabaja.persona, persona.ciudad, compañia.ciudad FROM persona
    JOIN trabaja ON (trabaja.persona=persona.nombre) 
    JOIN compañia ON (trabaja.compañia=compañia.nombre);

-- 14 explicar la siguiente consulta
  -- nombre de las personas que viven en la misma ciudad que trabajan

  SELECT persona.nombre FROM persona, trabaja, compañia 
    WHERE persona.nombre=trabaja.persona
    AND trabaja.compañia=compañia.nombre
    AND compañia.ciudad=persona.ciudad
    ORDER BY persona.nombre;

-- 15 nombre de la persona y nombre del supervisor

  SELECT persona, supervisor FROM supervisa;

-- 16 nombre de la persona, nombre de su supervisor y ciudades donde residen cada uno de ellos

  SELECT supervisa.persona, supervisa.supervisor, persona.ciudad FROM supervisa JOIN persona ON persona.nombre=supervisa.supervisor;

  SELECT DISTINCT supervisa.persona, persona.ciudad FROM supervisa
    JOIN persona ON supervisa.supervisor=persona.nombre;

  SELECT DISTINCT supervisa.supervisor, persona.ciudad FROM supervisa
    JOIN persona ON supervisa.supervisor=persona.nombre;


-- 17 numero de ciudades distintas que hay en la tabla compañia

  SELECT COUNT(DISTINCT ciudad) AS CiudadesDistintas FROM compañia;

-- 18 numero de ciudades distintas que hay en la tabla personas

  SELECT COUNT(DISTINCT ciudad) AS CiudadesDistintas FROM persona;

-- 19 nombre de las personas que trabajan para fagor

  SELECT persona FROM trabaja WHERE compañia='fagor';
  
-- 20 nombre de las personas que no trabajan para fagor
  
  SELECT persona FROM trabaja WHERE compañia!='fagor';

-- 21 numero de personas que trabajan para indra

  SELECT COUNT(DISTINCT persona) AS TrabajadoresIndra FROM trabaja WHERE compañia='indra';

-- 22 nombre de las personas que trabajan para fagor o para indra

  SELECT persona FROM trabaja WHERE compañia='fagor' OR compañia='indra';

-- 23 poblacion donde vive cada persona, su salario, su nombre y la compañia para la que trabaja. ordenar por nombre de persona y salario descendente

  SELECT ciudad.población, trabaja.salario, trabaja.persona, trabaja.compañia FROM trabaja 
    JOIN persona ON trabaja.persona=persona.nombre
    JOIN ciudad ON persona.ciudad=ciudad.nombre ORDER BY trabaja.persona DESC, trabaja.salario DESC;


  SELECT * FROM ciudad;
  SELECT * FROM compañia;
  SELECT * FROM persona;
  SELECT * FROM supervisa;
  SELECT * FROM trabaja;
