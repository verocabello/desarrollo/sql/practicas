USE ciclistas;

-- HOJA 1
-- 01 edades de los ciclistas (sin repetir)

  SELECT DISTINCT edad FROM ciclista;

-- 02 edades de los ciclistas de artiach

  SELECT DISTINCT edad FROM ciclista WHERE nomequipo='artiach';

-- 03 edades de los ciclistas de artiach o de amore vita

  SELECT DISTINCT edad FROM ciclista WHERE nomequipo='artiach' OR nomequipo='amore vita';

-- 04 dorsales de los ciclistas cuya edad sea menor que 25 o mayor de 30

  SELECT dorsal FROM ciclista WHERE edad<25 OR edad>30;

-- 05 dorsales de los ciclistas cuya edad este entre los 28 y 32 y adem�s que solo sean de banesto

  SELECT dorsal FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo='banesto';

-- 06 nombre de los ciclistas que el numero de caracteres del nombre sea mayor que 8

  SELECT nombre FROM ciclista WHERE LENGTH(nombre)>8;

-- 07 nombre y dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayusculas que debe mostrar el nombre en mayusculas

  SELECT nombre,dorsal,UPPER(nombre) AS NombreMayusculas FROM ciclista;

-- 08 ciclistas que han llevado el maillot MGE (amarillo) en alguna etapa

  SELECT DISTINCT ciclista.nombre FROM ciclista 
    JOIN lleva ON ciclista.dorsal=lleva.dorsal WHERE `código`='MGE';

-- 09 nombre de los puertos cuya altura sea mayor que 1500

  SELECT nompuerto FROM puerto WHERE altura>1500;

-- 10 dorsal de ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura este entre 1800 y 3000

  SELECT dorsal FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000;

-- 11 dorsal de ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura este entre 1800 y 3000

  SELECT dorsal FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000;



SELECT * FROM ciclista;
SELECT * FROM equipo;
SELECT * FROM etapa;
SELECT * FROM lleva;
SELECT * FROM maillot;
SELECT * FROM puerto;