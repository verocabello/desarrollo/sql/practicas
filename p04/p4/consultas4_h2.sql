USE ciclistas;

-- HOJA 2
-- 01 numero de ciclistas que hay

  SELECT COUNT(*) AS NumeroTotalCiclistas FROM ciclista;

-- 02 numero de ciclistas que hay del equipo banesto

  SELECT COUNT(*) AS CiclistasBanesto FROM ciclista WHERE nomequipo='banesto';

-- 03 edad media de los ciclistas

  SELECT AVG(edad) AS EdadMediaCiclistas FROM ciclista;

-- 04 edad media de los ciclistas del equipo banesto

  SELECT AVG(edad) AS EdadMediaCiclistasBanesto FROM ciclista WHERE nomequipo='banesto';

-- 05 edad media de los ciclistas por cada equipo

  SELECT AVG(edad) AS EdadMediaCiclistasPorEquipo,nomequipo FROM ciclista GROUP BY nomequipo;

-- 06 numero de ciclistas por equipo

  SELECT COUNT(dorsal) AS NumeroDeDorsales,nomequipo FROM ciclista GROUP BY nomequipo;

-- 07 numero total de puertos

  SELECT COUNT(*) AS NumeroTotalPuertos FROM puerto;

-- 08 numero total de puertos mayores de 1500

  SELECT COUNT(*) AS PuertosMayoresDe1500 FROM puerto WHERE altura>1500;

-- 09 nombre de los equipos que tengan mas de 4 ciclistas

  SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(dorsal)>4;

-- 10 nombre de los equipos que tengan mas de 4 ciclistas cuya edad este entre 28 y 32

  SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(dorsal)>4;

-- 11 numero de etapas que ha ganado cada uno de los ciclistas

  SELECT dorsal,COUNT(DISTINCT numetapa) n FROM etapa GROUP BY dorsal;

-- 12 dorsal de los ciclistas que hayan ganado mas de 1 etapa

  SELECT dorsal,COUNT(DISTINCT numetapa) n FROM etapa GROUP BY dorsal HAVING n>1;


SELECT * FROM ciclista;
SELECT * FROM equipo;
SELECT * FROM etapa;
SELECT * FROM lleva;
SELECT * FROM maillot;
SELECT * FROM puerto;