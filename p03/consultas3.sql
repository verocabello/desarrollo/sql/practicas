﻿USE practica3;

-- 01 numero de empleados de cada departamento. utilizar group by para agrupar por departamento

  SELECT emple.dept_no, depart.dnombre, COUNT(emp_no) AS NumeroEmpleados FROM emple JOIN depart ON emple.dept_no=depart.dept_no GROUP BY dept_no;

-- 02 departamentos con más de 5 empleados. utilizar group by para agrupar por departamento y having para estableces la condición sobre grupos

  SELECT emple.dept_no, depart.dnombre FROM emple JOIN depart ON emple.dept_no=depart.dept_no GROUP BY dept_no HAVING (COUNT(emp_no))>5;

-- 03 hallar la medida de los salarios de cada departamento. utilizar funcion avg y group by

  SELECT emple.dept_no, depart.dnombre, AVG(salario) FROM emple JOIN depart ON emple.dept_no=depart.dept_no GROUP BY dept_no;

-- 04 nombre de los empleados vendedores del departamento 'ventas'. nombre de departamento ventas, oficio vendedor

  SELECT emple.apellido FROM emple JOIN depart ON emple.dept_no=depart.dept_no WHERE emple.oficio='vendedor' AND depart.dnombre='ventas';

-- 05 número de vendedores del departamento 'ventas'. utilizar funcion count sobre la consulta anterior

  SELECT COUNT(emple.apellido) AS NumeroVendedores FROM emple JOIN depart ON emple.dept_no=depart.dept_no WHERE emple.oficio='vendedor' AND depart.dnombre='ventas';

-- 06 oficios de los empleados del departamento 'ventas'

  SELECT DISTINCT emple.oficio FROM emple JOIN depart ON emple.dept_no=depart.dept_no WHERE depart.dnombre='ventas';

-- 07 numero de empleados de cada departamento cuyo oficio se 'empleado'. tabla emple, utilizar group by para agrupar por departamento.clausula where con oficio 'empleado'

  SELECT dept_no, COUNT(DISTINCT apellido) AS NumeroEmpleados FROM emple WHERE oficio='empleado' GROUP BY dept_no;

-- 08 departamento con más empleados

  SELECT dept_no, COUNT(*) AS NumeroEmpleados FROM emple GROUP BY dept_no ORDER BY NumeroEmpleados DESC LIMIT 1;

-- 09 departamentos cuya suma de salarios sea mayor que la media de los salarios de todos los empleados

   SELECT AVG(salario) FROM emple;

   SELECT dept_no FROM (
          SELECT dept_no,SUM(salario) AS SumaSalario FROM emple GROUP BY dept_no HAVING SumaSalario > (
                        SELECT AVG(salario) FROM emple))c1;

   SELECT dept_no, SUM(salario) FROM emple GROUP BY dept_no;

-- 10 suma de salarios para cada oficio

  SELECT oficio,SUM(salario) FROM emple GROUP BY oficio;

-- 11 suma de salarios de cada oficio del departamento de 'ventas'

  SELECT depart.dnombre, emple.oficio, SUM(salario) FROM emple JOIN depart ON emple.dept_no=depart.dept_no WHERE depart.dnombre='ventas' GROUP BY emple.oficio;

-- 12 numero de departamento que tenga mas empleados cuyo oficio sea empleado

  SELECT dept_no, COUNT(*) AS NumeroEmpleados FROM emple WHERE oficio='empleado' GROUP BY dept_no ORDER BY NumeroEmpleados DESC LIMIT 1;

-- 13 numero de oficios distintos de cada departamento

  SELECT dept_no, COUNT(DISTINCT oficio) FROM emple GROUP BY dept_no;

-- 14 departamentos que tengan más de dos personas trabajando en la misma profesión

  SELECT DISTINCT dept_no FROM emple GROUP BY oficio, dept_no HAVING (COUNT(apellido))>1;

-- 15 visualizar suma de unidades por estantería. tabla herramientas

  SELECT estanteria, SUM(unidades) AS NumeroUnidades FROM herramientas GROUP BY estanteria;

-- 16 estanteria con más unidades de la tabla herramientas. con totales y sin totales

  SELECT estanteria, SUM(unidades) AS NumeroUnidades FROM herramientas GROUP BY estanteria ORDER BY NumeroUnidades DESC LIMIT 1;

-- 17 numero de medicos que pertenecen a cada hospital, ordenado por numero descendente de hospital

  SELECT cod_hospital, COUNT(DISTINCT apellidos) AS NumeroMedicos FROM medicos GROUP BY cod_hospital ORDER BY cod_hospital DESC;

-- 18 el nombre de especialidades que tiene cada hospital

  SELECT cod_hospital, especialidad FROM medicos GROUP BY cod_hospital, especialidad; 

-- 19 que aparezca por cada hospital y en cada especialidad el número de médicos. partir de la consulta anterior y utilizar group by

  SELECT cod_hospital, especialidad, COUNT(dni) n FROM medicos GROUP BY cod_hospital, especialidad;

-- 20 obtener por cada hospital el número de empleados

  SELECT cod_hospital, COUNT(dni) FROM personas GROUP BY cod_hospital;

-- 21 obtener por cada especialidad el número de trabajadores

  SELECT especialidad, COUNT(dni) FROM medicos GROUP BY especialidad;

-- 22 especialidad que tenga más médicos

  SELECT especialidad, COUNT(*) w FROM medicos GROUP BY especialidad HAVING w=(SELECT MAX(n) FROM (
    SELECT especialidad, COUNT(*) n FROM medicos GROUP BY especialidad) c1);

  SELECT MAX(n) FROM (
    SELECT especialidad, COUNT(*) n FROM medicos GROUP BY especialidad) c1;

-- 23 cual es el nombre del hospital que tiene mayor número de plazas

  SELECT nombre FROM (
    SELECT nombre, MAX(num_plazas) FROM hospitales) c1;

  SELECT nombre, MAX(num_plazas) FROM hospitales;
  
-- 24 las diferentes estanterías de la tabla herramientas ordenados por estanteria descendente

  SELECT DISTINCT estanteria FROM herramientas ORDER BY estanteria DESC;

-- 25 cuantas unidades tiene cada estantería

  SELECT estanteria, SUM(unidades) AS NumeroUnidades FROM herramientas GROUP BY estanteria;

-- 26 las estanterias que tengan mas de 15 unidades

  SELECT estanteria FROM (
    SELECT estanteria, SUM(unidades) AS NumeroUnidades FROM herramientas GROUP BY estanteria) c1 WHERE c1.NumeroUnidades>15;

-- 27 cual es la estanteria que tiene más unidades

  SELECT estanteria, MAX(c1.NumeroUnidades) FROM (
    SELECT estanteria, SUM(unidades) AS NumeroUnidades FROM herramientas GROUP BY estanteria) c1;

-- 28 mostrar los datos del departamento que no tiene ningun empleado. tablas emple y depart

  SELECT * FROM depart LEFT JOIN emple USING (dept_no) WHERE emple.dept_no IS NULL;

-- 29 numero de empleados de cada departamento. en la salida se debe mostrar también los departamentos que no tienen ningún empleado
  
  SELECT d.dept_no,COUNT(e.dept_no) FROM emple e RIGHT JOIN
      depart d USING(dept_no)  GROUP BY e.dept_no; 

-- 30 obtener la suma de salarios de cada departamento, mostrando dept_no, suma de salarios y dnombre. mostrar tambien los departamentos que no tienen asignados empleados

  SELECT depart.dept_no, SUM(emple.salario), depart.dnombre FROM emple RIGHT JOIN depart ON emple.dept_no=depart.dept_no GROUP BY dept_no;

-- 31 utilizar la función ifnull en la consulta anterior para que en el caso de que un departamento no tenga empleados, aparezca como suma de salarios el valor 0

  SELECT depart.dept_no, IFNULL(SUM(emple.salario), 0), depart.dnombre FROM emple RIGHT JOIN depart ON emple.dept_no=depart.dept_no GROUP BY dept_no;

-- 32 numero de medicos que pertenecen a cada hospital, mostrando cod_hospital, nombre y numero de medicos. deben aparecer también los datos de los hospitales que no tienen medicos

  SELECT hospitales.cod_hospital, hospitales.nombre, COUNT(dni) FROM hospitales JOIN medicos USING(cod_hospital) GROUP BY medicos.cod_hospital;
  



SELECT * FROM depart;
SELECT * FROM emple;
SELECT * FROM herramientas;
SELECT * FROM hospitales;
SELECT * FROM medicos;
SELECT * FROM personas;